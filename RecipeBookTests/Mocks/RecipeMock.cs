﻿using RecipeBook.Models;
using System;
using System.Collections.ObjectModel;

namespace RecipeBookTests.Mocks
{
    public class RecipeMock : IRecipe
    {
        public TimeSpan CookTime { get; set; } = new TimeSpan(0, 5, 0);
        public Cuisine Cuisine { get; set; } = new Cuisine() { Id = 3 };
        public string Description { get; set; } = "This is a Mock Recipe";
        public int Id { get; set; } = 1;
        public string Instructions { get; set; } = "Mock a recipe up";
        public string Name { get; set; } = "Mock Recipe";
        public TimeSpan PrepTime { get; set; } = new TimeSpan(0, 10, 0);
        public ObservableCollection<IRecipeIngredient> RecipeIngredients { get; set; } =
            new ObservableCollection<IRecipeIngredient>() { new RecipeIngredientMock() };

    }
}
