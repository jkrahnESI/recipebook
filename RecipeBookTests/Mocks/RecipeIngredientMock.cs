﻿using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RecipeBookTests.Mocks
{
    public class RecipeIngredientMock : IRecipeIngredient
    {
        public string Amount { get; set; } = "3 cups";
        public Ingredient Ingredient { get; set; } = new Ingredient() { Id = 1 };
        public bool Optional { get; set; } = false;
        public string Prep { get; set; } = "prep it up";
    }
}
