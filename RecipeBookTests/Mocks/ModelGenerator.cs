﻿using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace RecipeBookTests.Mocks
{
    public static class ModelGenerator
    {
        public static Recipe CreateRecipe()
        {
            var recipe = new Recipe()
            {
                Id = 1,
                Name = "test recipe",
                Description = "equality test",
                Instructions = "do a test",
                PrepTime = new TimeSpan(0, 10, 0),
                CookTime = new TimeSpan(10, 0, 0),
                Cuisine = CreateCuisine(),
                RecipeIngredients = new ObservableCollection<IRecipeIngredient>()
                {
                    CreateRecipeIngredient(),
                    CreateRecipeIngredient()
                }
            };
            
            return recipe;
        }

        public static IngredientType CreateIngredientType()
        {
            return new IngredientType { Id = 1, Type = "type" };
        }

        public static IngredientType CreateIngredientType(int id)
        {
            return new IngredientType { Id = id, Type = "type" };
        }

        public static IngredientType CreateIngredientType(string type)
        {
            return new IngredientType { Id = 5, Type = type };
        }
        public static IngredientType CreateIngredientType(int id, string type)
        {
            return new IngredientType { Id = id, Type = type };
        }

        public static Ingredient CreateIngredient()
        {
            var ingredient = new Ingredient()
            {
                Id = 1,
                Name = "name",
                IsStaple = true,
                Type = CreateIngredientType(1, "type")
            };
            return ingredient;
        }

        public static RecipeIngredient CreateRecipeIngredient()
        {
            var ri = new RecipeIngredient()
            {
                Amount = "amount",
                Ingredient = CreateIngredient(),
                Optional = false,
                Prep = "prep"
            };
            return ri;
        }

        public static Cuisine CreateCuisine()
        {
            return new Cuisine() { Id = 1, Name = "cuisine" };
        }
    }
}
