﻿using Xunit;
using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using RecipeBookTests.Mocks;

namespace RecipeBook.Models.Tests
{
    public class RecipeTests
    {
        [Fact()]
        public void EqualsTest()
        {
            Recipe recipe1 = ModelGenerator.CreateRecipe();
            Recipe recipe2 = ModelGenerator.CreateRecipe();
            Assert.True(recipe1.Equals(recipe2));
        }

        [Fact()]
        public void EqualityOperatorTest()
        {
            Recipe recipe1 = ModelGenerator.CreateRecipe();
            Recipe recipe2 = ModelGenerator.CreateRecipe();
            Assert.True(recipe1 == recipe2);
        }

        [Fact()]
        public void RecipeswithDifferentIdsAreNotEqualTest()
        {
            Recipe recipe1 = ModelGenerator.CreateRecipe();
            recipe1.Id = 20;
            Recipe recipe2 = ModelGenerator.CreateRecipe();
            Assert.False(recipe1 == recipe2);
        }

        [Fact()]
        public void RecipeswithDifferentRecipeIngredientsAreNotEqualTewt()
        {
            Recipe recipe1 = ModelGenerator.CreateRecipe();
            recipe1.RecipeIngredients[0].Ingredient.Id = 20;
            Recipe recipe2 = ModelGenerator.CreateRecipe();
            Assert.True(recipe1 != recipe2);
        }

        [Fact()]
        public void InequalityOperatorTest()
        {
            Recipe recipe1 = ModelGenerator.CreateRecipe();
            Recipe recipe2 = ModelGenerator.CreateRecipe();
            Assert.False(recipe1 != recipe2);
        }

        [Fact()]
        public void GetHashCodeTest()
        {
            Recipe recipe1 = ModelGenerator.CreateRecipe();
            Recipe recipe2 = ModelGenerator.CreateRecipe();
            Assert.True(recipe1.GetHashCode() == recipe2.GetHashCode());
        }
    }
}