﻿using Xunit;
using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Text;
using RecipeBookTests.Mocks;

namespace RecipeBook.Models.Tests
{
    public class IngredientTypeTests
    {
        [Fact()]
        public void EqualsTest()
        {
            var type1 = ModelGenerator.CreateIngredientType();
            var type2 = ModelGenerator.CreateIngredientType();
            Assert.True(type1.Equals(type2));
        }

        [Fact()]
        public void TypesWithDifferentIdsAreNotEqual()
        {
            var type1 = ModelGenerator.CreateIngredientType(1);
            var type2 = ModelGenerator.CreateIngredientType(2);
            Assert.False(type1.Equals(type2));
        }

        [Fact()]
        public void TypesWithDifferentNamesAreNotEqual()
        {
            var type1 = ModelGenerator.CreateIngredientType("name");
            var type2 = ModelGenerator.CreateIngredientType("differentname");
            Assert.False(type1.Equals(type2));
        }

        [Fact()]
        public void TypesWithDifferentIdsAndNamesAreNotEqual()
        {
            var type1 = ModelGenerator.CreateIngredientType(1, "name");
            var type2 = ModelGenerator.CreateIngredientType(2, "differentname");
            Assert.False(type1.Equals(type2));
        }


        [Fact()]
        public void GetHashCodeTest()
        {
            var type1 = ModelGenerator.CreateIngredientType();
            var type2 = ModelGenerator.CreateIngredientType();
            Assert.True(type1.GetHashCode() == type2.GetHashCode());
        }

        [Fact()]
        public void TypesWithDifferentIdsHashCodesAreNotEqual()
        {
            var type1 = ModelGenerator.CreateIngredientType(1);
            var type2 = ModelGenerator.CreateIngredientType(2);
            Assert.False(type1.GetHashCode() == type2.GetHashCode());
        }

        [Fact()]
        public void TypesWithDifferentNamesHashCodesAreNotEqual()
        {
            var type1 = ModelGenerator.CreateIngredientType("name");
            var type2 = ModelGenerator.CreateIngredientType("differentname");
            Assert.False(type1.GetHashCode() == type2.GetHashCode());
        }

        [Fact()]
        public void TypesWithDifferentIdsAndNamesHashCodesAreNotEqual()
        {
            var type1 = ModelGenerator.CreateIngredientType(1, "name");
            var type2 = ModelGenerator.CreateIngredientType(2, "differentname");
            Assert.False(type1.GetHashCode() == type2.GetHashCode());
        }
    }
}