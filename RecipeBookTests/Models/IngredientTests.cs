﻿using Xunit;
using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Text;
using RecipeBookTests.Mocks;

namespace RecipeBook.Models.Tests
{
    public class IngredientTests
    {
        [Fact()]
        public void EqualsTest()
        {
            var ingredient1 = ModelGenerator.CreateIngredient();
            var ingredient2 = ModelGenerator.CreateIngredient();
            Assert.True(ingredient1.Equals(ingredient2));
        }

        [Fact()]
        public void GetHashCodeTest()
        {
            var ingredient1 = ModelGenerator.CreateIngredient();
            var ingredient2 = ModelGenerator.CreateIngredient();
            Assert.True(ingredient1.GetHashCode() == ingredient2.GetHashCode());
        }
    }
}