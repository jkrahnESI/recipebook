﻿using Xunit;
using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Text;
using RecipeBookTests.Mocks;

namespace RecipeBook.Models.Tests
{
    public class CuisineTests
    {
        [Fact()]
        public void EqualsTest()
        {
            var cuisine1 = ModelGenerator.CreateCuisine();
            var cuisine2 = ModelGenerator.CreateCuisine();
            Assert.True(cuisine1.Equals(cuisine2));
        }

        [Fact()]
        public void GetHashCodeTest()
        {
            var cuisine1 = ModelGenerator.CreateCuisine();
            var cuisine2 = ModelGenerator.CreateCuisine();
            Assert.True(cuisine1.GetHashCode() == cuisine2.GetHashCode());
        }
    }
}