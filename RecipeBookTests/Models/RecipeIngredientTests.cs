﻿using Xunit;
using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Text;
using RecipeBookTests.Mocks;

namespace RecipeBook.Models.Tests
{
    public class RecipeIngredientTests
    {
        [Fact()]
        public void EqualsTest()
        {
            var ri1 = ModelGenerator.CreateRecipeIngredient();
            var ri2 = ModelGenerator.CreateRecipeIngredient();
            Assert.True(ri1.Equals(ri2));
        }

        [Fact()]
        public void GetHashCodeTest()
        {
            var ri1 = ModelGenerator.CreateRecipeIngredient();
            var ri2 = ModelGenerator.CreateRecipeIngredient();
            Assert.True(ri1.GetHashCode() == ri2.GetHashCode());
        }
    }
}