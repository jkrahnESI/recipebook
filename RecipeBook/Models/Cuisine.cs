﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Models
{
    public class Cuisine
    {
        private int _id;
        public int Id
        {
            get => _id;
            set { _id = value; }
        }

        private string _name;
        public string Name
        {
            get => _name;
            set { _name = value; }
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Cuisine))
            {
                return false;
            }
            Cuisine that = obj as Cuisine;
            return (this.Id == that.Id && this.Name == that.Name);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 23;
            hash = (hash * 7) + Id;
            hash = (hash * 7) + Name.GetHashCode();
            return hash;
        }
    }
}
