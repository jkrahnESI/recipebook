﻿using System;
using System.Collections.ObjectModel;

namespace RecipeBook.Models
{
    public interface IRecipe
    {
        TimeSpan CookTime { get; set; }
        Cuisine Cuisine { get; set; }
        string Description { get; set; }
        int Id { get; set; }
        string Instructions { get; set; }
        string Name { get; set; }
        TimeSpan PrepTime { get; set; }
        ObservableCollection<IRecipeIngredient> RecipeIngredients { get; set; }
    }
}