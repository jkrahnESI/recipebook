﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Models
{
    public class Recipe : IRecipe, INotifyPropertyChanged
    {
        public int Id { get; set; }
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Description { get; set; }
        public string Instructions { get; set; }
        public TimeSpan PrepTime { get; set; }
        public TimeSpan CookTime { get; set; }
        public Cuisine Cuisine { get; set; }
        public ObservableCollection<IRecipeIngredient> RecipeIngredients { get; set; }
        private int RecipeIngredientsHashCode 
        {
            get
            {
                int hash = 13;
                foreach (var ri in RecipeIngredients)
                {
                    hash = (hash * 7) + ri.GetHashCode();
                }
                return hash;
            }
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Recipe))
            {
                return false;
            }
            Recipe that = obj as Recipe;
            return (
                this.Id == that.Id &&
                this.Name == that.Name &&
                this.Description == that.Description &&
                this.Instructions == that.Instructions &&
                this.PrepTime == that.PrepTime &&
                this.CookTime == that.CookTime &&
                this.Cuisine.Equals(that.Cuisine) &&
                this.RecipeIngredients.SequenceEqual(that.RecipeIngredients)
                );
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 19;
            hash = (hash * 13) + Id;
            hash = (hash * 13) + Name.GetHashCode();
            hash = (hash * 13) + Description.GetHashCode();
            hash = (hash * 13) + Instructions.GetHashCode();
            hash = (hash * 13) + PrepTime.GetHashCode();
            hash = (hash * 13) + CookTime.GetHashCode();
            hash = (hash * 13) + Cuisine.GetHashCode();
            hash = (hash * 13) + RecipeIngredientsHashCode;
            
            return hash;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
