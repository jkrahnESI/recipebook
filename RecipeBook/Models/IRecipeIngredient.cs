﻿namespace RecipeBook.Models
{
    public interface IRecipeIngredient
    {
        string Amount { get; set; }
        Ingredient Ingredient { get; set; }
        bool Optional { get; set; }
        string Prep { get; set; }
    }
}