﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Models
{
    public class IngredientType
    {
        public int Id { get; set; } = 0;
        public string Type { get; set; } = "";

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is IngredientType))
            {
                return false;
            }
            IngredientType that = obj as IngredientType;
            return (this.Id == that.Id && this.Type == that.Type);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 31;
            hash = (hash * 7) + Id;
            hash = (hash * 7) + Type.GetHashCode();
            return hash;
        }
    }
}
