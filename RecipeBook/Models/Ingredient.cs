﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Models
{
    public class Ingredient
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = "";
        public IngredientType Type { get; set; } = new IngredientType();
        public bool IsStaple { get; set; } = false;

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Ingredient))
            {
                return false;
            }
            Ingredient that = obj as Ingredient;
            return (
                this.Id == that.Id &&
                this.Name == that.Name &&
                this.Type.Equals(that.Type) &&
                this.IsStaple == that.IsStaple
                );
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 37;
            hash = (hash * 7) + Id;
            hash = (hash * 7) + Name.GetHashCode();
            hash = (hash * 7) + Type.GetHashCode();
            hash = (hash * 7) + IsStaple.GetHashCode();
            return hash;
        }
    }
}
