﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Models
{
    public class RecipeIngredient : IRecipeIngredient, INotifyPropertyChanged
    {
        public RecipeIngredient()
        {
            Ingredient = new Ingredient();
            Amount = "";
            Prep = "";
            Optional = false;
        }
        private Ingredient _ingredient;

        public Ingredient Ingredient
        {
            get => _ingredient;
            set
            {
                _ingredient = value;
                OnPropertyChanged("Ingredient");
            }
        }

        private string _amount;
        public string Amount
        {
            get => _amount;
            set
            {
                _amount = value;
                OnPropertyChanged("Amount");
            }
        }

        private string _prep;
        public string Prep
        {
            get => _prep;
            set
            {
                _prep = value;
                OnPropertyChanged("Prep");
            }
        }

        private bool _optional;
        public bool Optional
        {
            get => _optional;
            set
            {
                _optional = value;
                OnPropertyChanged("Optional");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is RecipeIngredient))
            {
                return false;
            }
            RecipeIngredient that = obj as RecipeIngredient;
            return (
                this.Ingredient.Equals(that.Ingredient) &&
                this.Amount == that.Amount &&
                this.Prep == that.Prep &&
                this.Optional == that.Optional
            );
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            int hash = 41;
            hash = (hash * 7) + Ingredient.GetHashCode();
            hash = (hash * 7) + Amount.GetHashCode();
            hash = (hash * 7) + Ingredient.GetHashCode();
            hash = (hash * 7) + Prep.GetHashCode();
            hash = (hash * 7) + Optional.GetHashCode();

            return hash;
        }
    }
}
