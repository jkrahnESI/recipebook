﻿using RecipeBook.Models;
using RecipeBook.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RecipeBook.ViewModels
{
    public class RecipeIngredientViewModel : INotifyPropertyChanged
    {

        public RecipeIngredientViewModel(RecipeIngredient recipeIngredient)
        {
            _recipeIngredient = recipeIngredient;
        }

        private RecipeIngredient _recipeIngredient;
        public RecipeIngredientViewModel()
        {
            _recipeIngredient.Ingredient = new Ingredient()
            {
                Type = new IngredientType()
            };
        }

        public bool Optional
        {
            get => _recipeIngredient.Optional;
            set
            {
                _recipeIngredient.Optional = value;
                OnPropertyChanged("Optional");
            }
        }

        public Ingredient Ingredient
        {
            get
            {
                if (_recipeIngredient.Ingredient == null)
                    return new Ingredient();
                else
                    return ItemsSources.Ingredients.Single(i => i.Id == _recipeIngredient.Ingredient.Id);
            }
            set
            {
                if (value != null)
                {
                    _recipeIngredient.Ingredient = ItemsSources.Ingredients.Single(i => i.Id == value.Id);
                }
                else
                {
                    _recipeIngredient.Ingredient = ItemsSources.Ingredients.Single(i => i.Name == "");
                }
                OnPropertyChanged("Ingredient");
            }
        }

        public string Amount
        {
            get => _recipeIngredient.Amount;
            set 
            {
                _recipeIngredient.Amount = value;
                OnPropertyChanged("Amount");
            }
        }

        public string Prep
        {
            get => _recipeIngredient.Prep;
            set 
            {
                _recipeIngredient.Prep = value;
                OnPropertyChanged("Prep");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

    }
}
