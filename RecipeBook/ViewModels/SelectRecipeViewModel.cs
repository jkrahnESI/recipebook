﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using ExtensionMethods.StringExtensions;
using RecipeBook.Utilities;
using RecipeBook.Models;
using System.Windows.Input;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace RecipeBook.ViewModels
{
    public class SelectRecipeViewModel :  INotifyPropertyChanged
    {
        public SelectRecipeViewModel()
        {
            RefreshCommand = new RelayCommand(o => RefreshRecipes("RefreshButton"));
            UpdateCommand = new RelayCommand(o => UpdateRecipe("UpdateButton"));
            InitializeViewModel();
        }

        private void InitializeViewModel()
        {
            ItemsSources.PopulateUneditedRecipes();
            AllRecipes = DataFactory.GetAllWholeRecipes(); // TODO might be faster to copy the obscoll somehow
            Cuisines = DataFactory.GetAllCuisines();
            RecipeInfoViewModels = new ObservableCollection<RecipeInfoViewModel>();
            foreach (Recipe recipe in AllRecipes)
            {
                RecipeInfoViewModels.Add(new RecipeInfoViewModel(recipe, Cuisines));
            }
            
        }

        public ICommand RefreshCommand { get; set; }
        public ICommand UpdateCommand { get; set; }

        private void RefreshRecipes(object sender)
        {
            int selectedRecipeId = SelectedRecipe.Id;
            InitializeViewModel();
            SelectedRecipe = _allRecipes.Single(r => r.Id == selectedRecipeId);
            OnPropertyChanged("FilteredRecipes");
            OnPropertyChanged("SelectedRecipe");
            OnPropertyChanged("SelectedRecipeInfoViewModel");
        }
        
        private void UpdateRecipe(object sender)
        {
            SelectedRecipeInfoViewModel.RecipeIngredientsViewModel.SelectedItem = null;
            var uneditedRecipe = ItemsSources.UneditedRecipes.Single(r => r.Id == SelectedRecipe.Id);
            if (SelectedRecipe.Equals(uneditedRecipe))
            {
                return;
            }
            //OnPropertyChanged("SelectedRecipe");
            if (SelectedRecipe.RecipeIngredients.SequenceEqual(uneditedRecipe.RecipeIngredients))
            {
                DataFactory.UpdateRecipe(SelectedRecipe);
                SelectedRecipe = _allRecipes.Single(r => r.Id == SelectedRecipe.Id);
                OnPropertyChanged("SelectedRecipe");
                return;
            }
            //TODO: Minimize queries for RecipeIngredients by comparing uneditedRecipe and SelectedRecipe

            foreach (RecipeIngredient ri in uneditedRecipe.RecipeIngredients)
            {
                DataFactory.DeleteRecipeIngredient(ri, SelectedRecipe.Id);
            }

            foreach (RecipeIngredient ri in SelectedRecipe.RecipeIngredients)
            {
                DataFactory.AddRecipeIngredient(ri, SelectedRecipe.Id);
            }
            SelectedRecipe = _allRecipes.Single(r => r.Id == SelectedRecipe.Id);
            OnPropertyChanged("FilteredRecipes");
            OnPropertyChanged("SelectedRecipe");
            OnPropertyChanged("SelectedRecipeInfoViewModel");


        }

        public ObservableCollection<Recipe> FilteredRecipes
        {
            get
            {
                if (_searchString == "" || _searchString == null)
                    return _allRecipes;
                else
                    return new ObservableCollection<Recipe>(_allRecipes.Where(n => n.Name.Contains(_searchString, StringComparison.OrdinalIgnoreCase)));
            }
            set { OnPropertyChanged("FilteredRecipes"); }
        }

        private ObservableCollection<Cuisine> _cuisines;
        public ObservableCollection<Cuisine> Cuisines
        {
            get => _cuisines;
            set { _cuisines = value; }
        }

        private Recipe _selectedRecipe;
        public Recipe SelectedRecipe
        {
            get => _selectedRecipe;
            set 
            {
                if (value != null)
                {
                    _selectedRecipe = _allRecipes.Single(r => r.Id == value.Id);
                    OnPropertyChanged("SelectedRecipeInfoViewModel");
                }
            }
        }

        private RecipeInfoViewModel _selectedRecipeInfoViewModel;
        public RecipeInfoViewModel SelectedRecipeInfoViewModel
        {
            get
            {
                if (_selectedRecipe != null)
                {
                    return _recipeInfoViewModels.Single(r => r.Id == _selectedRecipe.Id);
                }
                return _selectedRecipeInfoViewModel;
            }
            set 
            { 
                _selectedRecipeInfoViewModel = value;
                OnPropertyChanged("SelectedRecipeInfoViewModel");
            }
        }


        private ObservableCollection<Recipe> _allRecipes;
        public ObservableCollection<Recipe> AllRecipes
        {
            get => _allRecipes;
            set 
            { 
                if (_allRecipes != null)
                {
                    foreach(var recipe in _allRecipes)
                    {
                        recipe.PropertyChanged -= PropertyChanged;
                    }
                }
                if (value != null)
                {
                    foreach (var recipe in value)
                    {
                        recipe.PropertyChanged += PropertyChanged;
                    }
                    _allRecipes = value;
                }
                OnPropertyChanged("AllRecipes");
                OnPropertyChanged("FilteredRecipes");
            }
        }
        private ObservableCollection<RecipeInfoViewModel> _recipeInfoViewModels;
        public ObservableCollection<RecipeInfoViewModel> RecipeInfoViewModels
        {
            get => _recipeInfoViewModels;
            set 
            {
                _recipeInfoViewModels = value;
                OnPropertyChanged("SelectedRecipeInfoViewModel");
                OnPropertyChanged("RecipeInfoViewModels");
            }
        }

        private string _searchString;
        public string SearchString
        {
            get => _searchString;
            set
            {
                _searchString = value;
                OnPropertyChanged("FilteredRecipes");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
