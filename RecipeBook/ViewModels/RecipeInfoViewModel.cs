﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using RecipeBook.Models;
using RecipeBook.Utilities;

namespace RecipeBook.ViewModels
{
    public class RecipeInfoViewModel : INotifyPropertyChanged
    {

        public RecipeInfoViewModel(Recipe recipe, ObservableCollection<Cuisine> cuisines)
        {
            _recipe = recipe;
            _recipeIngredientsViewModel = new RecipeIngredientsViewModel(RecipeIngredients);
        }
        private Recipe _recipe;
        public Recipe Recipe 
        {
            get => _recipe;
            set
            {
                _recipe = value;
            }
        }
        private RecipeIngredientsViewModel _recipeIngredientsViewModel;
        public RecipeIngredientsViewModel RecipeIngredientsViewModel
        {
            get => _recipeIngredientsViewModel;
            set { _recipeIngredientsViewModel = value; }
        }

        public string Name
        {
            get => _recipe.Name;
            set { _recipe.Name = value; }
        }
        public int Id
        {
            get => _recipe.Id;
        }

        public string Description
        {
            get => _recipe.Description;
            set { _recipe.Description = value; }
        }
        public string Instructions
        {
            get => _recipe.Instructions;
            set { _recipe.Instructions = value; }
        }

        public TimeSpan PrepTime
        {
            get => _recipe.PrepTime;
            set { _recipe.PrepTime = value; }
        }
        public TimeSpan CookTime
        {
            get => _recipe.CookTime;
            set { _recipe.CookTime = value; }
        }

        public Cuisine Cuisine
        {
            get => ItemsSources.Cuisines.Single(c => c.Id == _recipe.Cuisine.Id);
            set 
            { 
                _recipe.Cuisine = value;
                OnPropertyChanged("Cuisine");
            }
        }

        public ObservableCollection<IRecipeIngredient> RecipeIngredients
        {
            get => _recipe.RecipeIngredients;
            set { _recipe.RecipeIngredients = value; }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
