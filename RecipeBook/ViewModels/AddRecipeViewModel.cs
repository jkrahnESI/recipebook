﻿using RecipeBook.Utilities;
using System.ComponentModel;

namespace RecipeBook.ViewModels
{
    public class AddRecipeViewModel : INotifyPropertyChanged
    {
        public AddRecipeViewModel()
        {
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
