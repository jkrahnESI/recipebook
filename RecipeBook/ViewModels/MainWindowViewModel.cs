﻿using RecipeBook.Utilities;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;

namespace RecipeBook.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<TabItem> TabViewModels { get; set; }

        public MainWindowViewModel()
        {
            ItemsSources.PopulateCuisines();
            ItemsSources.PopulateIngredients();
            TabViewModels = new ObservableCollection<TabItem>()
            {
                new TabItem(){
                    Content = new Views.SelectRecipeView()
                    {
                        DataContext = new SelectRecipeViewModel()
                    },
                    Header = "Select Recipe"
                },
                new TabItem()
                {
                    Content = new Views.AddRecipeView() 
                    {
                        DataContext = new AddRecipeViewModel()
                    },
                    Header = "Add Recipe"
                }
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
