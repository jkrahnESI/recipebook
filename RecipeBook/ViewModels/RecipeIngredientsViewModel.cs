﻿using RecipeBook.Models;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace RecipeBook.ViewModels
{
    public class RecipeIngredientsViewModel : INotifyPropertyChanged
    {
        public RecipeIngredientsViewModel(ObservableCollection<IRecipeIngredient> recipeIngredients)
        {
            InsertCommand = new RelayCommand(o => InsertIngredient("InsertButton"));
            AddCommand = new RelayCommand(o => AddIngredient("AddButton"));
            DeleteCommand = new RelayCommand(o => DeleteIngredient("DeleteButton"));
            _recipeIngredients = recipeIngredients;
            _recipeIngredientViewModels = new ObservableCollection<RecipeIngredientViewModel>();
            foreach (RecipeIngredient recipeIngredient in _recipeIngredients)
            {
                _recipeIngredientViewModels.Add(new RecipeIngredientViewModel(recipeIngredient));
            }
        }

        private ObservableCollection<IRecipeIngredient> _recipeIngredients;
        public ObservableCollection<IRecipeIngredient> RecipeIngredients
        {
            get => _recipeIngredients;
            set 
            {
                _recipeIngredients = value;
                OnPropertyChanged("RecipeIngredients");
            }
        }
        private ObservableCollection<RecipeIngredientViewModel> _recipeIngredientViewModels;
        public ObservableCollection<RecipeIngredientViewModel> RecipeIngredientViewModels
        {
            get => _recipeIngredientViewModels;
            set
            {
                _recipeIngredientViewModels = value;
                OnPropertyChanged("RecipeIngredientViewModels");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        public ICommand InsertCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        private RecipeIngredientViewModel _selectedItem;
        public RecipeIngredientViewModel SelectedItem
        {
            get => _selectedItem;
            set { _selectedItem = value; }
        }

        private void InsertIngredient(object sender)
        {
            var recipeIngredient = new RecipeIngredient()
            {
                Ingredient = new Ingredient()
                { Name = "" }
            };
            int insertIndex = _recipeIngredientViewModels.IndexOf(SelectedItem);
            _recipeIngredients.Insert(insertIndex, recipeIngredient);
            _recipeIngredientViewModels.Insert(insertIndex, new RecipeIngredientViewModel(recipeIngredient));
            OnPropertyChanged("RecipeIngredients");
            OnPropertyChanged("RecipeIngredientViewModels");
        }

        private void AddIngredient(object sender)
        {
            var recipeIngredient = new RecipeIngredient()
            {
                Ingredient = new Ingredient()
                { Name = "" }
            };
            _recipeIngredients.Add(recipeIngredient);
            _recipeIngredientViewModels.Add(new RecipeIngredientViewModel(recipeIngredient));
            OnPropertyChanged("RecipeIngredients");
            OnPropertyChanged("RecipeIngredientViewModels");
        }

        private void DeleteIngredient(object sender)
        {
            int itemIndex = _recipeIngredientViewModels.IndexOf(SelectedItem);
            _recipeIngredients.RemoveAt(itemIndex);
            _recipeIngredientViewModels.RemoveAt(itemIndex);
            OnPropertyChanged("RecipeIngredients");
            OnPropertyChanged("RecipeIngredientViewModels");
        }
    }
}
