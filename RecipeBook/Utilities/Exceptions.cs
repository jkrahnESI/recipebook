﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Utilities
{
    public class RecipeBookException : System.Exception
    {
        public RecipeBookException()
        { }

        public RecipeBookException(string message) : base(message)
        { }
    }

    public class IncorrectIdException : RecipeBookException
    { 
        public IncorrectIdException()
        { }

        public IncorrectIdException(string message) : base(message)
        { }


    }
}
