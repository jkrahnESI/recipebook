﻿using System.Collections.Generic;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.SqlServer.Server;
using System.Collections.ObjectModel;
using RecipeBook.Models;
using System;
using System.Linq;

namespace RecipeBook.Utilities
{
    public static class DataFactory
    {


        public static ObservableCollection<Cuisine> GetAllCuisines()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.GetAllCuisines, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            var cuisineList = new ObservableCollection<Cuisine>();
            conn.Open();
            using var reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                var cuisine = new Cuisine()
                {
                    Id = Convert.ToInt32(reader["cuisine_id"].ToString()),
                    Name = reader["cuisine_name"].ToString()
                };
                cuisineList.Add(cuisine);
            }
            conn.Close();
            return cuisineList;
        }

        public static ObservableCollection<Ingredient> GetAllIngredients()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.GetAllIngredients, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            var ingredientColl = new ObservableCollection<Ingredient>();
            conn.Open();
            using var reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                var ingredient = new Ingredient()
                {
                    Id = Convert.ToInt32(reader["ingredient_id"].ToString()),
                    Type = new IngredientType()
                    {
                        Id = Convert.ToInt32(reader["ingredient_type_id"].ToString()),
                        Type = reader["ingredient_type"].ToString()
                    },
                    Name = reader["ingredient_name"].ToString(),
                    IsStaple = bool.Parse(reader["staple"].ToString())
                };
                ingredientColl.Add(ingredient);
            }
            conn.Close();
            return ingredientColl;
        }

        internal static void DeleteRecipeIngredient(RecipeIngredient ri, int recipeId)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.DeleteRecipeIngredient, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };

            sqlCommand.Parameters.Add("@recipe_id", SqlDbType.NVarChar).Value = recipeId;
            sqlCommand.Parameters.Add("@ingredient_id", SqlDbType.NVarChar).Value = ri.Ingredient.Id;
            conn.Open();
            sqlCommand.ExecuteNonQuery();
            conn.Close();
        }

        internal static void AddRecipeIngredient(RecipeIngredient ri, int recipeId)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.AddRecipeIngredient, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };

            sqlCommand.Parameters.Add("@recipe_id", SqlDbType.NVarChar).Value = recipeId;
            sqlCommand.Parameters.Add("@ingredient_id", SqlDbType.NVarChar).Value = ri.Ingredient.Id;
            sqlCommand.Parameters.Add("@ingredient_amount", SqlDbType.NVarChar).Value = ri.Amount;
            sqlCommand.Parameters.Add("@ingredient_prep", SqlDbType.NVarChar).Value = ri.Prep;
            sqlCommand.Parameters.Add("@ingredient_optional", SqlDbType.Bit).Value = ri.Optional;
            conn.Open();
            sqlCommand.ExecuteNonQuery();
            conn.Close();
        }

        public static void Test3()
        {
            var stringArgs = new List<string>() { "Salt", "Water" };
            List<SqlDataRecord> args = NVarCharList(stringArgs, "recipe_name");

            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = QueryIngredientList(args, "sp_Get_Recipes_On_Hand", conn);
            conn.Open();
            using var reader = sqlCommand.ExecuteReader();
            var list = new List<string>();
            while (reader.Read())
                list.Add(reader.GetString(0));
            conn.Close();

        }

        public static ObservableCollection<string> GetAllRecipeNames()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.GetAllRecipeNames, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            var nameList = new ObservableCollection<string>();
            conn.Open();
            using var reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                nameList.Add(reader.GetString(0));
            }
            conn.Close();
            return nameList;
        }

        public static void UpdateRecipe(IRecipe recipe)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.UpdateRecipe, conn)
            {
                CommandType = CommandType.StoredProcedure,
            };
            sqlCommand.Parameters.Add("@recipe_id", SqlDbType.Int).Value = recipe.Id;
            sqlCommand.Parameters.Add("@recipe_name", SqlDbType.NVarChar).Value = recipe.Name;
            sqlCommand.Parameters.Add("@recipe_description", SqlDbType.NVarChar).Value = recipe.Description;
            sqlCommand.Parameters.Add("@recipe_instructions", SqlDbType.NVarChar).Value = recipe.Instructions;
            sqlCommand.Parameters.Add("@recipe_prep_time", SqlDbType.Time).Value = recipe.PrepTime;
            sqlCommand.Parameters.Add("@recipe_cook_time", SqlDbType.Time).Value = recipe.CookTime;
            sqlCommand.Parameters.Add("@recipe_cuisine_id", SqlDbType.Int).Value = recipe.Cuisine.Id;
            
            conn.Open();
            sqlCommand.ExecuteNonQuery();

            conn.Close();
        }

        public static void UpdateRecipeIngredients(IRecipe recipe)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.UpdateRecipeIngredients, conn)
            {
                CommandType = CommandType.StoredProcedure,
            };
            sqlCommand.Parameters.Add("@recipe_id", SqlDbType.Int).Value = recipe.Id;

        }

        public static ObservableCollection<Recipe> GetAllWholeRecipes()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.GetAllWholeRecipes, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            conn.Open();
            using var reader = sqlCommand.ExecuteReader();
            int previousId = -1;
            var recipeColl = new ObservableCollection<Recipe>();
            Recipe recipe = new Recipe();
            bool firstLoop = true;
            while (reader.Read())
            {
                int id = Convert.ToInt32(reader["recipe_id"].ToString());
                if (id != previousId)
                {
                    if (!firstLoop)
                    {
                        recipeColl.Add(recipe);
                    }
                    recipe = new Recipe();
                    recipe = GetRecipe(reader);
                    recipe.Cuisine = GetCuisine(reader);
                    recipe.RecipeIngredients = new ObservableCollection<IRecipeIngredient>();
                }
                recipe.RecipeIngredients.Add(GetRecipeIngredient(reader));
                firstLoop = false;
                previousId = id;
            }
            recipeColl.Add(recipe);
            conn.Close();
            return recipeColl;
        }

        private static Recipe GetRecipe(SqlDataReader reader)
        {
            Recipe recipe = new Recipe();
            recipe.Id = Convert.ToInt32(reader["recipe_id"].ToString());
            recipe.Name = reader["recipe_name"].ToString();
            recipe.Description = reader["recipe_description"].ToString();
            recipe.Instructions = reader["recipe_instructions"].ToString();
            recipe.PrepTime = TimeSpan.Parse(reader["recipe_prep_time"].ToString());
            recipe.CookTime = TimeSpan.Parse(reader["recipe_cook_time"].ToString());
            return recipe;
        }

        private static Cuisine GetCuisine(SqlDataReader reader)
        {
            Cuisine cuisine = new Cuisine()
            {
                Id = Convert.ToInt32(reader["cuisine_id"].ToString()),
                Name = reader["cuisine_name"].ToString()
            };
            return cuisine;
        }
        private static RecipeIngredient GetRecipeIngredient(SqlDataReader reader)
        {
            int ingredientId = Convert.ToInt32(reader["ingredient_id"].ToString());
            RecipeIngredient recipeIngredient = new RecipeIngredient()
            {
                Ingredient = ItemsSources.Ingredients.Single(i => i.Id == ingredientId),
                //Ingredient = new Ingredient
                //{
                //    Id = Convert.ToInt32(reader["ingredient_id"].ToString()),
                //    Name = reader["ingredient_name"].ToString(),
                //    Type = new IngredientType()
                //    {
                //        Id = Convert.ToInt32(reader["ingredient_type_id"].ToString()),
                //        Type = reader["ingredient_type"].ToString(),
                //    },
                //    IsStaple = bool.Parse(reader["staple"].ToString())
                //},
                Amount = reader["ingredient_amount"].ToString(),
                Prep = reader["ingredient_prep"].ToString(),
                Optional = bool.Parse(reader["ingredient_optional"].ToString())
            };
            return recipeIngredient;
        }

        public static Recipe GetWholeRecipeFromId(int id)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using SqlConnection conn = new SqlConnection(connectionString);
            using SqlCommand sqlCommand = new SqlCommand(StoredProcedures.GetWholeRecipeFromId, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add("@recipe_id", SqlDbType.Int).Value = id;
            conn.Open();
            using var reader = sqlCommand.ExecuteReader();
            Recipe recipe = new Recipe();
            bool firstLoop = true;
            while (reader.Read())
            {
                recipe.Id = Convert.ToInt32(reader["recipe_id"].ToString());
                if (recipe.Id != id)
                {
                    throw new IncorrectIdException("Recipe ID from database did not match desired ID");
                }
                if (firstLoop)
                {
                    recipe = new Recipe();
                    recipe = GetRecipe(reader);
                    recipe.Cuisine = GetCuisine(reader);
                    recipe.RecipeIngredients = new ObservableCollection<IRecipeIngredient>();
                    firstLoop = false;
                }
                recipe.RecipeIngredients.Add(GetRecipeIngredient(reader));
            }
            conn.Close();
            return recipe;
        }


        private static SqlCommand QueryIngredientList(List<SqlDataRecord> args, string sp, SqlConnection conn)
        {
            SqlCommand sqlCommand = new SqlCommand(sp, conn)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            sqlCommand.Parameters.Add(
                "@ingredient_names", SqlDbType.Structured).Value = args;
            sqlCommand.Parameters["@ingredient_names"].TypeName = "dbo.IngredientList";
            return sqlCommand;
        }

        private static List<SqlDataRecord> NVarCharList(IEnumerable<string> stringList, string column_name)
        {
            SqlMetaData metaData = new SqlMetaData(column_name, SqlDbType.NVarChar, 50);
            var args = new List<SqlDataRecord>();
            foreach (var arg in stringList)
            {
                var record = new SqlDataRecord(metaData);
                record.SetString(0, arg);
                args.Add(record);
            }
            return args;
        }

    }
}
