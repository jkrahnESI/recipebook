﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Utilities
{
    // Stored Procedure Names
    public static class StoredProcedures
    {
        public static string GetAllCuisines { get; } = "sp_Get_All_Cuisines";
        public static string GetAllIngredientTypes { get; } = "sp_Get_All_Ingredient_Types";
        public static string GetAllIngredients { get; } = "sp_Get_All_Ingredients";
        public static string GetAllRecipeNames { get; } = "sp_Get_All_Recipe_Names";
        public static string GetAllRecipes { get; } = "sp_Get_All_Recipes";
        public static string GetAllWholeRecipes { get; } = "sp_Get_All_Whole_Recipes";
        public static string GetRecipeId { get; } = "sp_Get_Recipe_Id";
        public static string GetRecipeIngredientsFromRecipeId { get; } = "sp_Get_Recipe_Ingredients_From_Recipe_Id";
        public static string GetRecipesFromIngredient { get; } = "sp_Get_Recipes_From_Ingredient";
        public static string GetRecipesOnHand { get; } = "sp_Get_Recipes_On_Hand";
        public static string GetRecipesWithSpecificIngredientList { get; } = "sp_Get_Recipes_With_Specific_IngredientList";
        public static string GetWholeRecipeFromId { get; } = "sp_Get_Whole_Recipe_From_Id";
        public static string GetWholeRecipeFromName { get; } = "sp_Get_Whole_Recipe_From_Name";
        public static string UpdateRecipe { get; } = "sp_Update_Recipe";
        public static string UpdateRecipeIngredients { get; } = "sp_Update_Recipe_Ingredients";
        public static string AddRecipeIngredient { get; } = "sp_Add_Recipe_Ingredient";
        public static string DeleteRecipeIngredient { get; } = "sp_Delete_Recipe_Ingredient";
        public static string UpdateRecipeIngredient { get; } = "sp_Update_Recipe_Ingredient";


    }
}
