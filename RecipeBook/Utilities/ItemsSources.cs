﻿using RecipeBook.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipeBook.Utilities
{
    public static class ItemsSources
    {
        public static ObservableCollection<Cuisine> Cuisines { get; set; }

        public static void PopulateCuisines()
        {
            Cuisines = DataFactory.GetAllCuisines();
        }

        public static ObservableCollection<Ingredient> Ingredients { get; set; }

        public static void PopulateIngredients()
        {
            Ingredients = DataFactory.GetAllIngredients();
        }

        public static ObservableCollection<Recipe> UneditedRecipes { get; set; }
        public static void PopulateUneditedRecipes()
        {
            UneditedRecipes = DataFactory.GetAllWholeRecipes();
        }
    }
}
